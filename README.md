# Quinto
Java applet game

The goal is to turn out all the lights on the grid. Click on any square to invert the color of that square and adjacent squares. Try to use a few clicks as possible!
